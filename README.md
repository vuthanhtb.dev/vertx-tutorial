## Setup
```
mvn io.reactiverse:vertx-maven-plugin::setup
```

## Running
```
mvn clean compile vertx:run
```

## Infinispan Cluster Manager (Java 11)
```
mvn clean package
```

```
java -jar target/vertx-tutorial-1.0-SNAPSHOT.jar -cluster -Djava.net.preferIPv4Stack=true -Dhttp.port=8090
```
