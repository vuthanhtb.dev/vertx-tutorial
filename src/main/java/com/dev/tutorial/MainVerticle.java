package com.dev.tutorial;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;

public class MainVerticle extends AbstractVerticle {

    private final JsonObject loadedConfig = new JsonObject();

    @Override
    public void start(Promise<Void> startPromise) {
        this.doConfig()
                .compose(this::storeConfig)
                .compose(this::deployVerticle)
                .andThen(startPromise);
    }

    private Future<Void> deployVerticle(Void unused) {
        DeploymentOptions opts = new DeploymentOptions().setConfig(this.loadedConfig);

        Future<String> helloVerticle = Future.future(promise -> vertx.deployVerticle(new HelloVerticle(), opts, promise));
        Future<String> webVerticle = Future.future(promise -> vertx.deployVerticle(new WebVerticle(), opts, promise));
        Future<String> databaseVerticle = Future.future(promise -> vertx.deployVerticle(new DatabaseVerticle(), opts, promise));
        return CompositeFuture.all(helloVerticle, webVerticle, databaseVerticle).mapEmpty();
    }

    private Future<Void> storeConfig(JsonObject config) {
        this.loadedConfig.mergeIn(config);
        return Future.succeededFuture();
    }

    private Future<JsonObject> doConfig() {
        ConfigStoreOptions configStoreOptions = new ConfigStoreOptions()
                .setType("file")
                .setFormat("json")
                .setConfig(new JsonObject().put("path", "config.json"));
        ConfigRetrieverOptions configRetrieverOptions = new ConfigRetrieverOptions().addStore(configStoreOptions);
        ConfigRetriever configRetriever = ConfigRetriever.create(this.vertx, configRetrieverOptions);
        return Future.future(configRetriever::getConfig);
    }
}
