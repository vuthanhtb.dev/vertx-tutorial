package com.dev.tutorial;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.ext.web.sstore.SessionStore;

import static com.dev.tutorial.Constants.TOPIC_HELLO_NAMED;
import static com.dev.tutorial.Constants.TOPIC_HELLO_VERTX;

public class WebVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        this.configureRouter().compose(this::startHttpServer).andThen(startPromise);
    }

    private Future<Router> configureRouter() {
        JsonObject dbConfig = config().getJsonObject("secret");
        String csrf = dbConfig.getString("csrf", "");
        String cors = dbConfig.getString("cors", "");

        Router router = Router.router(this.vertx);
        router.route().handler(LoggerHandler.create());
        router.route().handler(CorsHandler.create(cors));
        router.route().handler(CSRFHandler.create(this.vertx, csrf));

        SessionStore sessionStore = LocalSessionStore.create(this.vertx);
        router.route().handler(SessionHandler.create(sessionStore));

        router.get("/api/v1/tutorial").handler(this::vertxTutorial);
        router.get("/api/v1/tutorial/:name").handler(this::tutorialName);

        router.route().handler(StaticHandler.create("web").setIndexPage("index.html"));
        return Future.succeededFuture(router);
    }

    private Future<Void> startHttpServer(Router router) {
        JsonObject httpJson = config().getJsonObject("http");
        int httpPort = httpJson.getInteger("port");
        HttpServer server = this.vertx.createHttpServer().requestHandler(router);
        return Future.<HttpServer>future(promise -> server.listen(httpPort, promise)).mapEmpty();
    }

    private void tutorialName(RoutingContext ctx) {
        String name = ctx.pathParam("name");
        this.vertx.eventBus().request(TOPIC_HELLO_NAMED, name, reply -> ctx.request().response().end((String) reply.result().body()));
    }

    private void vertxTutorial(RoutingContext ctx) {
        this.vertx.eventBus().request(TOPIC_HELLO_VERTX, "", reply -> ctx.request().response().end((String) reply.result().body()));
    }
}
