package com.dev.tutorial;

public class Constants {
    public static String TOPIC_HELLO_VERTX = "hello.vertx.address";
    public static String TOPIC_HELLO_NAMED = "hello.named.address";

    public static final String LIST_ALL_TODOS_ADDR = "com.dev.tutorial.list_all_todos";
    public static final String ADD_TODO_ADD = "com.dev.tutorial.add_todo";
    public static final String GET_TODO_BY_ID_ADDR = "com.dev.tutorial.get_todo_by_id";
    public static final String UPDATE_TODO_ADDR = "com.dev.tutorial.update_todo";

    public static final String LIST_ALL_TODOS = "SELECT * FROM todos ORDER BY created ASC";
    public static final String GET_TODO_BY_ID = "SELECT * FROM todos WHERE id = ?";
    public static final String UPDATE_TODO = "UPDATE todos SET title = ?, description = ?, due_date = ?, complete = ? WHERE id = ? RETURNING *";
    public static final String ADD_TODO = "INSERT INTO todos (title, description, due_date, complete) VALUES (?, ?, ?, ?) RETURNING *";
}
