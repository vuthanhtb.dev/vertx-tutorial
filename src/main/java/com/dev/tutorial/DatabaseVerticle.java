package com.dev.tutorial;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;

import static com.dev.tutorial.Constants.*;

public class DatabaseVerticle extends AbstractVerticle {

    private SQLClient client;

    @Override
    public void start(Promise<Void> startPromise) {
        this.doDatabaseMigrations()
                .compose(this::configureSqlClient)
                .compose(this::configureEventBusConsumers)
                .andThen(startPromise);
    }

    private Future<Void> configureEventBusConsumers(Void unused) {
        vertx.eventBus().consumer(LIST_ALL_TODOS_ADDR).handler(this::listAllTodos);
        vertx.eventBus().consumer(GET_TODO_BY_ID_ADDR).handler(this::getTodoById);
        vertx.eventBus().consumer(UPDATE_TODO_ADDR).handler(this::updateTodo);
        vertx.eventBus().consumer(ADD_TODO_ADD).handler(this::addTodo);

        return Future.succeededFuture();
    }

    private void addTodo(Message<Object> msg) {
        if (msg.body() instanceof JsonObject) {
            JsonObject todo = (JsonObject) msg.body();
            if (todo.containsKey("title")) {
                JsonArray params = new JsonArray()
                        .add(todo.getString("title"))
                        .add(todo.getString("description", ""))
                        .add(todo.getBoolean("complete", false))
                        .add(todo.getString("due_date", null));
                getConnection(msg, params, ADD_TODO);
            } else {
                msg.fail(400, "Bad Request: Required field 'title' missing from todo item.");
            }
        } else {
            msg.fail(400, "Bad Request: You must supply a valid Todo item in the body of the request");
        }
    }

    private void updateTodo(Message<Object> msg) {
        if (msg.body() instanceof JsonObject) {
            JsonObject todo = (JsonObject) msg.body();
            JsonArray params = new JsonArray()
                    .add(todo.getString("title"))
                    .add(todo.getString("description", ""))
                    .add(todo.getString("due_date", null))
                    .add(todo.getBoolean("complete", false))
                    .add(todo.getString("id"));
            getConnection(msg, params, UPDATE_TODO);
        } else {
            msg.fail(400, "Bad Request: You must supply a valid Todo item in the body of the request");
        }
    }

    private void getTodoById(Message<Object> msg) {
        if (msg.body() instanceof String) {
            String id = (String) msg.body();
            JsonArray params = new JsonArray().add(id);
            getConnection(msg, params, GET_TODO_BY_ID);
        } else {
            msg.fail(400, "Bad Request, you must supply the Todo ID");
        }
    }

    private void getConnection(Message<Object> msg, JsonArray params, String func) {
        Future.future(client::getConnection)
                .compose(conn -> this.queryWithParameters(conn, func, params))
                .compose(this::mapToFirstResult)
                .andThen(res -> {
                    if (res.succeeded()) {
                        msg.reply(res.result());
                    } else {
                        msg.fail(500, res.cause().getLocalizedMessage());
                    }
                });
    }

    private void listAllTodos(Message<Object> msg) {
        Future.future(client::getConnection)
                .compose(conn -> this.queryWithParameters(conn, LIST_ALL_TODOS, new JsonArray()))
                .compose(this::mapToJsonArray)
                .andThen(res -> {
                    if (res.succeeded()) {
                        msg.reply(res.result());
                    } else {
                        msg.fail(500, res.cause().getLocalizedMessage());
                    }
                });
    }

    private Future<Void> configureSqlClient(Void unused) {
        this.client = JDBCClient.createShared(this.vertx, config().getJsonObject("db"));
        return Future.succeededFuture();
    }

    private Future<ResultSet> queryWithParameters(SQLConnection conn, String query, JsonArray params) {
        return Future.future(promise -> conn.queryWithParams(query, params, promise));
    }

    private Future<JsonObject> mapToFirstResult(ResultSet rs) {
        if (rs.getNumRows() >= 1) {
            return Future.succeededFuture(rs.getRows().get(0));
        }

        return Future.failedFuture("No results");
    }

    private Future<JsonArray> mapToJsonArray(ResultSet rs) {
        return Future.succeededFuture(new JsonArray(rs.getRows()));
    }

    private Future<Void> doDatabaseMigrations() {
        JsonObject dbConfig = config().getJsonObject("db");
        String url = dbConfig.getString("url");
        String user = dbConfig.getString("user");
        String password = dbConfig.getString("password");

        Flyway flyway = Flyway
                .configure()
                .dataSource(url, user, password)
                .load();

        try {
            flyway.migrate();
            return Future.succeededFuture();
        } catch (FlywayException e) {
            return Future.failedFuture(e.getMessage());
        }
    }
}
