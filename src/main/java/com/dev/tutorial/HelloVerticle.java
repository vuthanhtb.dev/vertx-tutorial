package com.dev.tutorial;

import io.vertx.core.AbstractVerticle;

import java.util.UUID;

import static com.dev.tutorial.Constants.TOPIC_HELLO_NAMED;
import static com.dev.tutorial.Constants.TOPIC_HELLO_VERTX;

public class HelloVerticle extends AbstractVerticle {

    private final String verticleId = UUID.randomUUID().toString();

    @Override
    public void start() {
        this.vertx.eventBus().consumer(TOPIC_HELLO_VERTX, message -> message.reply("Vertx Address"));

        this.vertx.eventBus().consumer(TOPIC_HELLO_NAMED, message -> {
            String name = (String) message.body();
            message.reply(String.format("Hello, %s from %s", name, verticleId));
        });
    }
}
